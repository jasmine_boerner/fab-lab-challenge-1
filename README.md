# Fab Lab Challenge 1

Shared and open repository for our first fab lab challenge in the MDEF program.

## Sun Thing

![](1.png)

We see this prototype as a way to explore alternative presents through cooking; both in space and in experience. It can also be an imagined tool for refugee tech and future realities within the confines of the climate crisis. And perhaps another space of reality for this project is educating children on imagining future kitchens and cooking experiences.

![](2.png)


![](3.png)

![](4.png)

We started to really try to understand how we could maximize the heat produced with our design and landed on creating 16 petals that would form a paraboloid- which would be attached with a ring on the bottom and a ring on top made with a pressfit design. 

![](5.png)

We had several issues with plywood and spent alot of time testing various kerf patterns- re-designing and even mixing two in one attempt. 

![](6.png)


![](7.png)

![](8.png)


![](9.png)

We didn't end up discovering the 'right' kerf and 'right' material but we were super glad to at least having a prototype this afternoon!

![](10.png)


![](11.png)

The End!
